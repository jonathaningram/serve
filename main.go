package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("file server listening on port %s", port)
	log.Fatal(http.ListenAndServe(":"+port, http.FileServer(http.Dir(dir))))
}
