# serve

## Installation

~~~ shell
go get bitbucket.org/jonathaningram/serve
~~~

## Serving

~~~ shell
cd /some/dir/with/static/files/to/serve
PORT=5000 serve
~~~
